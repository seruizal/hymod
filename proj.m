classdef proj
    
    
   properties 
    
      in 
      n
      wem
      Pbase
      LPSP
      ips


   end
   
   methods
      
       function obj = proj(in, n, wem, Pbase, LPSP, ips)
         obj.in = in;
         obj.n = n;
         obj.wem = wem;
         obj.Pbase = Pbase;
         obj.LPSP = LPSP;
         obj.ips = ips;

       end
            
   end
    
end