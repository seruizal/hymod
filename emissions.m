 function Em=emissions(xn,x,obj)   
        
        Nwt = xn(1);
        Npv = xn(2);
        Nbt = xn(3);
        Vol = xn(4);
        Pus = x(1:obj.r_l.N);
        Pdg = x(obj.r_l.N+1:2*obj.r_l.N);
        Pbatc1 = x(2*obj.r_l.N+1:3*obj.r_l.N);
        Pbatd1 = x(3*obj.r_l.N+1:4*obj.r_l.N);
        Phpb1 = x(4*obj.r_l.N+1:5*obj.r_l.N);
        Phpt1 = x(5*obj.r_l.N+1:6*obj.r_l.N);
        Pdcev = x(6*obj.r_l.N+1:7*obj.r_l.N);
        Pcev = x(7*obj.r_l.N+1:8*obj.r_l.N);
        Pcevnd = x(8*obj.r_l.N+1:9*obj.r_l.N);  
            %% Emissions
          
          %% Diesel emissions
          %% Due to construction 
          E_dgc=obj.dg.Edgcc*obj.dg.Pdgnom*obj.dg.Ndg;
          %% Due to operation
          Con_com=(1/3.78541).*((0.2461.*Pdg)+(0.084151*obj.dg.Ndg*obj.dg.Pdgnom.*ones(obj.r_l.Tf-obj.r_l.Ti,1))); %% Diesel fuel consumption [gal]
          Edgop=obj.dg.Edgopc*3.78541*sum(Con_com)*(8760/(obj.r_l.Tf-obj.r_l.Ti));
          Edg=sum(E_dgc)+sum(Edgop);

          %% Wind turbines emissions
          %% By construction
          Ewtc=obj.wt.Ewtcc*obj.wt.Pwtnom*Nwt;

          %% Photovoltaic panels emissions
          %% By construction
          Epvc=obj.pv.Epvcc*obj.pv.Ppvnom*Npv;

          %% Batteries emissions
          %% By construction
          Ebatc=obj.bt.Ebtcc*obj.bt.Enom*Nbt;


          %% Emmisions for hydro-pumped storage system
          %% By construction
          
          Ephc=obj.hp.Ehpcc*(max(Phpb1)+max(Phpt1));

           %% Yearly cost
                    
          Em=sum(E_dgc)+(sum(Ewtc)+sum(Epvc)+sum(Ebatc)+sum(Ephc))*((obj.prj_p.in*(obj.prj_p.in+1)^obj.prj_p.n)/(-1+(obj.prj_p.in+1)^obj.prj_p.n))+sum(Edgop);

