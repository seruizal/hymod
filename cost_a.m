function [LCOE En_c dif_c Em]=cost_a(xn,x,obj)

Pus = x(obj.r_l.N+1:2*obj.r_l.N);

LCOEn=0;
LCOEd=0;

for ii=1:obj.prj_p.n
    LCOEn1=[];
    LCOEd1=[];
    
    LCOEn1=cost(xn,x,obj)/((1+obj.prj_p.in)^ii);
    LCOEd1=sum(obj.r_l.Pload-Pus)/((1+obj.prj_p.in)^ii);
    
    LCOEn=LCOEn+LCOEn1;
    LCOEd=LCOEd+LCOEd1;
end

    LCOE=LCOEn/LCOEd;

    En_c = (cost(xn,x,obj)/sum(obj.r_l.Pload-Pus))/obj.r_l.N;
    
    dif_c = cost_d(obj.r_l.Pload-Pus,obj) - (cost(xn,x,obj)+obj.prj_p.wem*emissions(xn,x,obj));

end