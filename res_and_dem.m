classdef res_and_dem
    
    
   properties 
    
%% Resources and demand
      Ta
      Va
      Gd
      La
      
      %% Simulation parameters
      Ti 
      Tf 
      
      %% Output parameters
      N
      G
      Tm
      V
      Pload
      nd
      nfsv


   end
   
 
   
   methods
      
       function obj = res_and_dem(Ta, Va, Gd, La, Ti, Tf)
         obj.Ta = Ta;
         obj.Va = Va;
         obj.Gd = Gd;
         obj.La = La;
         obj.Ti = Ti;
         obj.Tf = Tf;
         obj.N = Tf-Ti; %% Number of steps in the simulation period
         obj.G = Gd(Ti+1:Tf);
         obj.Tm = Ta(Ti+1:Tf);
         obj.V = Va(Ti+1:Tf);
         obj.Pload = La(Ti+1:Tf);
         [obj.nd,obj.nfsv]=day(obj);
       end
       
       function [nd,nfsv]=day(obj)
         nd=floor(obj.N/24); %% Numero de dias en el horizonte de tiempo

          nfs=floor(nd/7); %% Numero de fines de semana
          nfsv=[];
          for jj=1:nfs
             nfsv=[nfsv;7*jj-1;7*jj];
          end

          nfsv=nfsv-3;
       end
            
 
   end 
end