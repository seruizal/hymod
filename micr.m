function varargout = micr(varargin)
%MICR M-file for micr.fig
%      MICR, by itself, creates a new MICR or raises the existing
%      singleton*.
%
%      H = MICR returns the handle to a new MICR or the handle to
%      the existing singleton*.
%
%      MICR('Property','Value',...) creates a new MICR using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to micr_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      MICR('CALLBACK') and MICR('CALLBACK',hObject,...) call the
%      local function named CALLBACK in MICR.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help micr

% Last Modified by GUIDE v2.5 25-Jan-2018 10:36:39

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @micr_OpeningFcn, ...
                   'gui_OutputFcn',  @micr_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before micr is made visible.
function micr_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)
%% Get Exchange Rate from web
% url = 'https://research.stlouisfed.org/fred2/';
% c = fred(url);
% series = 'DEXUSEU';
% startdate = '01/01/2007';
% enddate = '06/01/2007';
% d = fetch(c,series,startdate,enddate);
% usd2eur = d.Data(end,2);
usd2eur = 0.811795057;

% url1 = 'http://currencies.apps.grandtrunk.net/getlatest/usd/cop';
% usd2cop = str2num(webread(url1));
usd2cop = 2710.0271;

% url2 = 'http://currencies.apps.grandtrunk.net/getlatest/eur/cop';
% eur2cop = str2num(webread(url2));
eur2cop = 3338.31436;

    setappdata(0,'usd2eur',usd2eur);
    setappdata(0,'usd2cop',usd2cop);
    setappdata(0,'eur2cop',eur2cop);
    
    %% Default currency values
    setappdata(0,'curr1',1);
    setappdata(0,'currv1','USD');
    setappdata(0,'curr2',1);
    setappdata(0,'currv2','USD');
% Choose default command line output for micr
handles.output = hObject;
axes(handles.axes1);
imshow('wt.png');
axes(handles.axes2);
imshow('pv.png');
axes(handles.axes3);
imshow('dg.png');
axes(handles.axes4);
imshow('bat.png');
axes(handles.axes5);
imshow('hydro_pump_s.png');
axes(handles.axes6);
imshow('ev.png');
axes(handles.axes7);
imshow('load.png');
axes(handles.axes8);
imshow('temp.png');
axes(handles.axes9);
imshow('sol.png');
axes(handles.axes10);
imshow('wind.png');
% Update handles structure

setappdata(0,'flg1',0);
setappdata(0,'flg2',0);
setappdata(0,'flg3',0);
setappdata(0,'flg4',0);
setappdata(0,'flg5',0);
setappdata(0,'flg6',0);

load('statewt.mat');
Pwtnom=str2double(state.edi_wt1);
rws=str2double(state.edi_wt2);
ciw=str2double(state.edi_wt3);
cfw=str2double(state.edi_wt4);
cinv_wt=str2double(state.edi_wt5);
cinv_wtinv=str2double(state.edi_wt6);
cinv_fund=str2double(state.edi_wt7);
cmt_wt=str2double(state.edi_wt8);
Ewtcc=str2double(state.edi_wt9);
Awt=str2double(state.edi_wt10);

setappdata(0,'Pwtnom',Pwtnom);
setappdata(0,'rws',rws);
setappdata(0,'ciw',ciw);
setappdata(0,'cfw',cfw);
setappdata(0,'cinv_wt',cinv_wt);
setappdata(0,'cinv_wtinv',cinv_wtinv);
setappdata(0,'cinv_fund',cinv_fund);
setappdata(0,'cmt_wt',cmt_wt);
setappdata(0,'Ewtcc',Ewtcc);
setappdata(0,'Awt',Awt);

load('statepv.mat');

miu_pmax=str2double(state.edi_pv2);
Ga0=str2double(state.edi_pv3);
Pmax=str2double(state.edi_pv4);
NOCT=str2double(state.edi_pv5);
TM0=str2double(state.edi_pv6);
fpv=str2double(state.edi_pv7);
Nm=str2double(state.edi_pv8);
cinv_pv=str2double(state.edi_pv9);
cinv_pvinv=str2double(state.edi_pv10);
cmt_pv=str2double(state.edi_pv11);
Epvcc=str2double(state.edi_pv12);
Apv=str2double(state.edi_pv13);


setappdata(0,'miu_pmax',miu_pmax);
setappdata(0,'Ga0',Ga0);
setappdata(0,'Pmax',Pmax);
setappdata(0,'NOCT',NOCT);
setappdata(0,'TM0',TM0);
setappdata(0,'fpv',fpv);
setappdata(0,'Nm',Nm);
setappdata(0,'cinv_pv',cinv_pv);
setappdata(0,'cinv_pvinv',cinv_pvinv);
setappdata(0,'cmt_pv',cmt_pv);
setappdata(0,'Epvcc',Epvcc);
setappdata(0,'Apv',Apv);

load('statedg.mat');

Ndg = str2double(state.edi_dg1);
Pdgnom = str2double(state.edi_dg2);
ndiesel = str2double(state.edi_dg3);
E_dg_max=str2double(state.edi_dg4);
cost_l=str2double(state.edi_dg5);
cmt_dg = str2double(state.edi_dg6);
Edgcc = str2double(state.edi_dg7);
Edgopc = str2double(state.edi_dg8);
c_jan = str2double(state.edi_dg11);
c_feb = str2double(state.edi_dg12);
c_mar = str2double(state.edi_dg13);
c_apr = str2double(state.edi_dg14);
c_may = str2double(state.edi_dg15);
c_jun = str2double(state.edi_dg16);
c_jul = str2double(state.edi_dg17);
c_aug = str2double(state.edi_dg18);
c_sep = str2double(state.edi_dg19);
c_oct = str2double(state.edi_dg20);
c_nov = str2double(state.edi_dg21);
c_dec = str2double(state.edi_dg22);


setappdata(0,'Ndg',Ndg);
setappdata(0,'Pdgnom',Pdgnom);
setappdata(0,'ndiesel',ndiesel);
setappdata(0,'E_dg_max',E_dg_max);
setappdata(0,'cost_l',cost_l);
setappdata(0,'cmt_dg',cmt_dg);
setappdata(0,'Edgcc',Edgcc);
setappdata(0,'Edgopc',Edgopc);
setappdata(0,'c_jan',c_jan);
setappdata(0,'c_feb',c_feb);
setappdata(0,'c_mar',c_mar);
setappdata(0,'c_apr',c_apr);
setappdata(0,'c_may',c_may);
setappdata(0,'c_jun',c_jun);
setappdata(0,'c_jul',c_jul);
setappdata(0,'c_aug',c_aug);
setappdata(0,'c_sep',c_sep);
setappdata(0,'c_oct',c_oct);
setappdata(0,'c_nov',c_nov);
setappdata(0,'c_dec',c_dec);

load('statebt.mat');

Enom = str2double(state.edi_bt1);
sigma = str2double(state.edi_bt2);
nbatc = str2double(state.edi_bt3);
nbatd = str2double(state.edi_bt4);
Emin = str2double(state.edi_bt5);
Cch_nom = str2double(state.edi_bt6);
cinv_bt = str2double(state.edi_bt7);
cinv_btinv  = str2double(state.edi_bt8);
cmt_bt = str2double(state.edi_bt9);
Ebtcc = str2double(state.edi_bt10);
Abt = str2double(state.edi_bt11);

% 
setappdata(0,'Enom',Enom);
setappdata(0,'sigma',sigma);
setappdata(0,'nbatc',nbatc);
setappdata(0,'nbatd',nbatd);
setappdata(0,'Emin',Emin);
setappdata(0,'Cch_nom',Cch_nom);
setappdata(0,'cinv_bt',cinv_bt);
setappdata(0,'cinv_btinv',cinv_btinv );
setappdata(0,'cmt_bt',cmt_bt);
setappdata(0,'Ebtcc',Ebtcc);
setappdata(0,'Abt',Abt);

load('statehp.mat');

vol_min = str2double(state.edi_hp1);
sigma1 = str2double(state.edi_hp2);
np = str2double(state.edi_hp3);
nt = str2double(state.edi_hp4);
h = str2double(state.edi_hp5);
Q_max = str2double(state.edi_hp6);
cinv_p = str2double(state.edi_hp7);
cinv_t = str2double(state.edi_hp8);
cinv_V = str2double(state.edi_hp9);
cmt_hp = str2double(state.edi_hp10);
Ehpcc = str2double(state.edi_hp11);

setappdata(0,'vol_min',vol_min);
setappdata(0,'sigma1',sigma1);
setappdata(0,'np',np);
setappdata(0,'nt',nt);
setappdata(0,'h',h);
setappdata(0,'Q_max',Q_max);  
setappdata(0,'cinv_p',cinv_p);
setappdata(0,'cinv_t',cinv_t);
setappdata(0,'cinv_V',cinv_V);
setappdata(0,'cmt_hp',cmt_hp);
setappdata(0,'Ehpcc',Ehpcc);

load('stateev.mat');

Enomev = str2double(state.edi_ev1);
sigmaev = str2double(state.edi_ev2);
hsi = str2num(state.edi_ev3);
hpi = str2num(state.edi_ev4);
Eminev = str2double(state.edi_ev5);
Cch_nomev = str2double(state.edi_ev6);
cinv_ev = str2double(state.edi_ev7);
lEV2 = str2double(state.edi_ev8);
Nev = str2double(state.edi_ev9);
Nev1 = str2double(state.edi_ev10);
Nev2 = str2double(state.edi_ev11);
% 
setappdata(0,'Enomev',Enomev);
setappdata(0,'sigmaev',sigmaev);
setappdata(0,'hsi',hsi);
setappdata(0,'hpi',hpi);
setappdata(0,'Eminev',Eminev);
setappdata(0,'Cch_nomev',Cch_nomev);
setappdata(0,'cinv_ev',cinv_ev);
setappdata(0,'lEV2',lEV2);
setappdata(0,'Nev',Nev);
setappdata(0,'Nev1',Nev1);
setappdata(0,'Nev2',Nev2);
% rev2 = getappdata(0,'rev2');

guidata(hObject, handles);

% UIWAIT makes micr wait for user response (see UIRESUME)
% uiwait(handles.GUI1);


% --- Outputs from this function are returned to the command line.
function varargout = micr_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
flg2 = getappdata(0,'flg2');
if flg2==0
    irr1=[];
else
[irr path] = uigetfile('.xlsx'); 
irr1 = xlsread([path '/' irr]);
end

% Add your data to the handles structure
handles.irr=irr1;
guidata(hObject, handles)


% Gsol = importfile(irr, 1, 8760);
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
flg1 = getappdata(0,'flg1');
if flg1==0
    wind1=[];
else
[wind path] = uigetfile('.xlsx'); 
wind1 = xlsread([path '/' wind]);
end
% Add your data to the handles structure
handles.wind=wind1;
guidata(hObject, handles)

% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
flg2 = getappdata(0,'flg2');
if flg2==0
    tempa1=[];
else

[tempa path] = uigetfile('.xlsx'); 
tempa1 = xlsread([path '/' tempa]);
end
% Add your data to the handles structure
handles.tempa=tempa1;
guidata(hObject, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
flg6 = getappdata(0,'flg6');
if flg6==0
    rev21 =[];
else

[rev2 path] = uigetfile('.xlsx'); 
rev21 = xlsread([path '/' rev2]);

end
% Add your data to the handles structure
handles.rev2=rev21;
guidata(hObject, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Tf=str2double(get(hObject,'String'));
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Ti=str2double(get(hObject,'String'));
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
fig2 = wind_t();    %returns second GUI's figure handle
guidata(hObject, handles);
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
fig3 = solar_p();    %returns second GUI's figure handle

guidata(hObject, handles);
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.in=str2double(get(hObject,'String'));
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.n=str2double(get(hObject,'String'));
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.wem=str2double(get(hObject,'String'));
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Pbase=str2double(get(hObject,'String'));
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.LPSP=str2double(get(hObject,'String'));
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double


% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.ips=str2double(get(hObject,'String'));
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double


% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)

fig4 = diesel_g();    %returns second GUI's figure handle
guidata(hObject, handles);
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
fig5 = battery();    %returns second GUI's figure handle
guidata(hObject, handles);
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
fig6 = hydro_pump();    %returns second GUI's figure handle
guidata(hObject, handles);
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
fig7 = electric_veh();    %returns second GUI's figure handle
guidata(hObject, handles);

% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
Ti= str2double(get(handles.edit2,'String'));
Tf= str2double(get(handles.edit3,'String'));

setappdata(0,'Ti',Ti);
setappdata(0,'Tf',Tf);

La= getappdata(0,'Loadd');

% %%%% Project parameters%% Interest rate and proyect life-span
in = str2double(get(handles.edit4,'String'));
n = str2double(get(handles.edit5,'String'));
wem = str2double(get(handles.edit6,'String'));
Pbase = str2double(get(handles.edit7,'String'));
LPSP = str2double(get(handles.edit8,'String'));
ips = str2double(get(handles.edit9,'String'));

for jj=1:length(La)
    if La(jj)<=Pbase
        La(jj)=Pbase;
    end
end

Pwtnom = getappdata(0,'Pwtnom');
rws = getappdata(0,'rws');
ciw = getappdata(0,'ciw');
cfw = getappdata(0,'cfw');
cinv_wt = getappdata(0,'cinv_wt');
cinv_wtinv = getappdata(0,'cinv_wtinv');
cinv_fund = getappdata(0,'cinv_fund');
cmt_wt = getappdata(0,'cmt_wt');
Ewtcc = getappdata(0,'Ewtcc');
Awt = getappdata(0,'Awt');



miu_pmax =getappdata(0,'miu_pmax');
Ga0 =getappdata(0,'Ga0');
Pmax = getappdata(0,'Pmax');
NOCT = getappdata(0,'NOCT');
TM0 = getappdata(0,'TM0');
fpv = getappdata(0,'fpv');
Nm = getappdata(0,'Nm');
cinv_pv = getappdata(0,'cinv_pv');
cinv_pvinv = getappdata(0,'cinv_pvinv');
cmt_pv = getappdata(0,'cmt_pv');
Epvcc = getappdata(0,'Epvcc');
Apv = getappdata(0,'Apv');
% 
Ndg = getappdata(0,'Ndg');
Pdgnom = getappdata(0,'Pdgnom');
ndiesel = getappdata(0,'ndiesel');
E_dg_max = getappdata(0,'E_dg_max');
cost_l = getappdata(0,'cost_l');
cmt_dg = getappdata(0,'cmt_dg');
Edgcc = getappdata(0,'Edgcc');
Edgopc = getappdata(0,'Edgopc');
c_jan = getappdata(0,'c_jan');
c_feb = getappdata(0,'c_feb');
c_mar = getappdata(0,'c_mar');
c_apr = getappdata(0,'c_apr');
c_may = getappdata(0,'c_may');
c_jun = getappdata(0,'c_jun');
c_jul = getappdata(0,'c_jul');
c_aug = getappdata(0,'c_aug');
c_sep = getappdata(0,'c_sep');
c_oct = getappdata(0,'c_oct');
c_nov = getappdata(0,'c_nov');
c_dec = getappdata(0,'c_dec');

cost_c=[c_jan.*ones(31*24,1);c_feb.*ones(28*24,1);...
    c_mar.*ones(31*24,1);c_apr.*ones(30*24,1);...
    c_may.*ones(31*24,1);c_jun.*ones(30*24,1);...
    c_jul.*ones(31*24,1);c_aug.*ones(31*24,1);...
    c_sep.*ones(30*24,1);c_oct.*ones(31*24,1);...
    c_nov.*ones(30*24,1);c_dec.*ones(31*24,1)];

Enom = getappdata(0,'Enom');
sigma = getappdata(0,'sigma');
nbatc = getappdata(0,'nbatc');
nbatd = getappdata(0,'nbatd');
Emin = getappdata(0,'Emin');
Cch_nom = getappdata(0,'Cch_nom');
cinv_bt = getappdata(0,'cinv_bt');
cinv_btinv = getappdata(0,'cinv_btinv');
cmt_bt = getappdata(0,'cmt_bt');
Ebtcc = getappdata(0,'Ebtcc');
Abt = getappdata(0,'Abt');

vol_min = getappdata(0,'vol_min');
sigma1 = getappdata(0,'sigma1');
np = getappdata(0,'np');
nt = getappdata(0,'nt');
h = getappdata(0,'h');
Q_max = getappdata(0,'Q_max');  
cinv_p = getappdata(0,'cinv_p');
cinv_t = getappdata(0,'cinv_t');
cinv_V = getappdata(0,'cinv_V');
cmt_hp = getappdata(0,'cmt_hp');
Ehpcc = getappdata(0,'Ehpcc');
% 
Enomev = getappdata(0,'Enomev');
sigmaev = getappdata(0,'sigmaev');
hsi = getappdata(0,'hsi');
hpi = getappdata(0,'hpi');
Eminev = getappdata(0,'Eminev');
Cch_nomev = getappdata(0,'Cch_nomev');
cinv_ev = getappdata(0,'cinv_ev');
lEV2 = getappdata(0,'lEV2');
Nev = getappdata(0,'Nev');
Nev1 = getappdata(0,'Nev1');
Nev2 = getappdata(0,'Nev2');
rev2 = getappdata(0,'rev2');

flg1 = getappdata(0,'flg1');
flg2 = getappdata(0,'flg2');
flg3 = getappdata(0,'flg3');
flg4 = getappdata(0,'flg4');
flg5 = getappdata(0,'flg5');
flg6 = getappdata(0,'flg6');



if flg1==0
    Va=zeros(1,Tf-Ti);
else
    Va=handles.wind;
end


if flg2==0
    Gd=zeros(Tf-Ti,1);
    Ta=zeros(Tf-Ti,1);
else
    Gd=handles.irr; 
    Ta=handles.tempa; 
end



rho=1000; %% Water density [kg/m^3]
g=9.8; %% Gravity [N]
        

%% Calculation
r_l = res_and_dem(Ta, Va, Gd, La, Ti, Tf);
prj_p = proj(in, n, wem, Pbase, LPSP, ips);
wt = wind_turbine(rws, Pwtnom, ciw, cfw, cinv_wt, ...
     cinv_wtinv, cinv_fund, cmt_wt, Ewtcc, Awt, r_l);
pv=solar_panel(miu_pmax, Ga0, Pmax, NOCT, TM0, fpv, Nm, cinv_pv,...
   cinv_pvinv, cmt_pv, Epvcc, Apv, r_l);
dg = diesel(Ndg, Pdgnom, ndiesel, E_dg_max , cost_c, cost_l, cmt_dg, Edgcc, Edgopc);
bt= bat(sigma, nbatc, nbatd, Enom, Emin, Cch_nom, cinv_bt, cinv_btinv, cmt_bt, Ebtcc, Abt);
hp = hydro_p(np, nt, vol_min, sigma1, h,...
    Q_max, cinv_p, cinv_t, cinv_V, cmt_hp, Ehpcc);
ev = electric_v(sigmaev, Enomev, Eminev, Cch_nomev, hsi, hpi, rev2,...
     lEV2, Nev, Nev1, Nev2, cinv_ev);
EVd = demandEV(hsi, hpi, rev2, lEV2, r_l);

stor = storage(sigma, sigmaev, Enom, Emin, vol_min, sigma1, Enomev, Eminev, r_l);


xsol= optim(r_l, prj_p, wt, pv, dg, bt, hp, ev, EVd, stor);

flg4 = getappdata(0,'flg4');
flg5 = getappdata(0,'flg5');
flg6 = getappdata(0,'flg6');

%% Other variables
Psol=xsol.Npv*pv.Ppv;
Pwind=xsol.Nwt*wt.Pwt;
Pren=Psol+Pwind';
Pcevt=xsol.Pcev+xsol.Pcevnd;

Pdcev2 = EVd.Mb*xsol.Pdcev; 
Pdcev2g = EVd.Md*xsol.Pdcev;

rho=1000; 
g=9.8; 
 

if flg4==0
    Ebat=zeros(length(xsol.Pbatc)+1,1);
else
Ebat = (xsol.Nbt*stor.b1) + stor.Ar*(xsol.Pbatc-xsol.Pbatd);
end

if flg5==0
    hp.h = 0;
    hp.np = 1;
    hp.nt = 1;
    Qur1=zeros(length(xsol.Phpt)+1,1);
else
nhp = 1e3/(rho*g*hp.h);
Qur1 = stor.v1 + stor.Ar1*((xsol.Phpb-xsol.Phpt)*3600.*nhp);
end

if flg6==0
    Ebatev=zeros(length(xsol.Pdcev)+1,1);
else
Ebatev=ev.Nev2 * stor.bev + stor.Ar*(xsol.Pcev-xsol.Pdcev);
end

Qur=(rho*g*hp.h*2.7778e-7).*Qur1;

setappdata(0,'Nwt',xsol.Nwt);
setappdata(0,'Npv',xsol.Npv);
setappdata(0,'Nbt',xsol.Nbt);
setappdata(0,'Vol',xsol.Vol);
setappdata(0,'Pload',r_l.Pload); 
setappdata(0,'Pus',xsol.Pus);
setappdata(0,'Pdg',xsol.Pdg);
setappdata(0,'Pbatc',xsol.Pbatc);
setappdata(0,'Pbatd',xsol.Pbatd);
setappdata(0,'Ebat',Ebat);
setappdata(0,'Phpb',xsol.Phpb);
setappdata(0,'Phpt',xsol.Phpt);
setappdata(0,'Qur',Qur);
setappdata(0,'Pdcev',xsol.Pdcev);
setappdata(0,'Pcev',xsol.Pcev);
setappdata(0,'Ebatev',Ebatev);
setappdata(0,'Pcevnd',xsol.Pcevnd);
setappdata(0,'fval',xsol.fval);
setappdata(0,'LCOE',xsol.LCOE);
setappdata(0,'En_c',xsol.En_c);
setappdata(0,'dif_c',xsol.dif_c);
setappdata(0,'Psol',Psol);
setappdata(0,'Pwind',Pwind);
setappdata(0,'Pren',Pren);
setappdata(0,'Pcevt',Pcevt);
setappdata(0,'Pdcev2',Pdcev2);
setappdata(0,'Pdcev2g',Pdcev2g);

fig9=output2();
handles9 = guidata(fig9);

fig8 = output1();
handles8 = guidata(fig8); 
Nwtm=num2str(xsol.Nwt);
set(handles8.edit1,'String',Nwtm);
Npvm=num2str(xsol.Npv);
set(handles8.edit2,'String',Npvm);
Nbtm=num2str(xsol.Nbt);
set(handles8.edit3,'String',Nbtm);
Volm=num2str(xsol.Vol);
set(handles8.edit4,'String',Volm);
Phpm=num2str(max(xsol.Phpb)/hp.np);
set(handles8.edit5,'String',Phpm);
Phtm=num2str(max(xsol.Phpt)/hp.nt);
set(handles8.edit6,'String',Phtm);


curr1 = getappdata(0,'curr1');
currv1 = getappdata(0,'currv1');

curr2 = getappdata(0,'curr2');
currv2 = getappdata(0,'currv2');

csm_i=xsol.fval*1e-6;
lcoem_i=xsol.LCOE;
En_cm_i=xsol.En_c;
dif_cm_i=xsol.dif_c*1e-6;

setappdata(0,'csm',csm_i);
setappdata(0,'lcoem',lcoem_i);
setappdata(0,'En_cm',En_cm_i);
setappdata(0,'dif_cm',dif_cm_i);

ex = getappdata(0,'ex');

csm1=csm_i*ex;
csm=num2str(csm1);
set(handles8.edit7,'String',[csm ' M' currv2]);
lcoem1=lcoem_i*ex;
lcoem=num2str(lcoem1);
set(handles8.edit8,'String',[lcoem ' ' currv2]);
En_cm1=En_cm_i*ex;
En_cm=num2str(En_cm1);
set(handles8.edit9,'String',[En_cm ' ' currv2]);
dif_cm1=dif_cm_i*ex;
dif_cm=num2str(dif_cm1);
set(handles8.edit10,'String',[dif_cm ' M' currv2]);

Emm=num2str(round(xsol.Em*0.00110231));
set(handles8.edit11,'String',[Emm ' ton']);
Psolt=sum(Psol);
Pwindt=sum(Pwind);
Pdgt=sum(xsol.Pdg);
bar([Psolt,Pwindt,Pdgt],'stacked');
ax=gca;
ax.XTick=[1:3];
ax.XTickLabel=['Solar ';'Wind  ';'Diesel'];
ax.XTickLabelRotation = 50;
ylabel('Energy [kWh]');

% set(gca, 'fontsize', 14);
% legend('Solar power','Wind power','Diesel power');
% ylabel('Power [kW]');



        


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Determine the selected data set.
str = get(hObject, 'String');
val = get(hObject,'Value');
% Set current data to the selected data set.
switch str{val};
case 'USD' 
    setappdata(0,'curr1',1);
    setappdata(0,'currv1',str{val});
case 'EUR' 
    setappdata(0,'curr1',2);
    setappdata(0,'currv1',str{val});
case 'COP' 
    setappdata(0,'curr1',3);
    setappdata(0,'currv1',str{val});
end

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2
str = get(hObject, 'String');
val = get(hObject,'Value');

% Set current data to the selected data set.
switch str{val};
case 'Load file' 
    [loade path] = uigetfile('.xlsx'); 
    load1 = xlsread([path '/' loade]);
    setappdata(0,'Loadd',load1);
case 'Generate' 
    prompt = {'Enter the households number:'};
    dlg_title = 'Load generation';
    num_lines = 1;
    defaultans = {'158'};
    Nh1 = inputdlg(prompt,dlg_title,num_lines,defaultans);
    Nh = str2num(Nh1{:}); 
    load1=load_d(Nh);
    setappdata(0,'Loadd',load1);
end



% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
estado = get(handles.checkbox4,'Value');

if estado == 1
    setappdata(0,'flg4',1);
elseif estado == 0
    setappdata(0,'flg4',0);
end  
% Hint: get(hObject,'Value') returns toggle state of checkbox4


% --- Executes on button press in checkbox5.
function checkbox5_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
estado = get(handles.checkbox5,'Value');

if estado == 1
    setappdata(0,'flg5',1);
elseif estado == 0
    setappdata(0,'flg5',0);
end 
% Hint: get(hObject,'Value') returns toggle state of checkbox5


% --- Executes on button press in checkbox6.
function checkbox6_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
estado = get(handles.checkbox6,'Value');

if estado == 1
    setappdata(0,'flg6',1);
elseif estado == 0
    setappdata(0,'flg6',0);
end 
% Hint: get(hObject,'Value') returns toggle state of checkbox6


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
estado = get(handles.checkbox1,'Value');

if estado == 1
    setappdata(0,'flg1',1);
elseif estado == 0
    setappdata(0,'flg1',0);
end  

% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
estado = get(handles.checkbox2,'Value');

if estado == 1
    setappdata(0,'flg2',1);
elseif estado == 0
    setappdata(0,'flg2',0);
end 
% Hint: get(hObject,'Value') returns toggle state of checkbox2


% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
estado = get(handles.checkbox3,'Value');

if estado == 1
    setappdata(0,'flg3',1);
elseif estado == 0
    setappdata(0,'flg3',0);
end 
% Hint: get(hObject,'Value') returns toggle state of checkbox3
