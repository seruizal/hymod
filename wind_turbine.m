classdef wind_turbine
    
    
   properties 
      
      r_l
      Pwtnom
      rws 
      ciw 
      cfw
      cinv_wt
      cinv_wtinv
      cinv_fund
      cmt_wt
      Ewtcc
      Awt 
      Pwt

   end
   
   methods
      
       function obj = wind_turbine(rws, Pwtnom, ciw, cfw, cinv_wt, ...
         cinv_wtinv, cinv_fund, cmt_wt, Ewtcc, Awt, r_l)
         obj.rws = rws;
         obj.Pwtnom = Pwtnom;
         obj.ciw = ciw;
         obj.cfw = cfw;
         obj.cinv_wt = cinv_wt;
         obj.cinv_wtinv = cinv_wtinv;
         obj.cinv_fund = cinv_fund;
         obj.cmt_wt = cmt_wt;
         obj.Ewtcc = Ewtcc;
         obj.Awt = Awt;
         obj.r_l = r_l; 
         obj.Pwt = power(obj);
       end
       
       function Pwt = power(obj)
          flg1 = getappdata(0,'flg1');
          Ti = getappdata(0,'Ti');
          Tf = getappdata(0,'Tf');
          
        if flg1==0
            Pwt=zeros(1,Tf-Ti);
        else
        k = 2;
        Pwt = zeros(1, length(obj.r_l.V));
        
        for jj = 1:length(obj.r_l.V) 
            if (obj.r_l.V(jj)>=obj.ciw) && (obj.r_l.V(jj)<=obj.rws)   
               Pwt(jj)=obj.Pwtnom*(((obj.r_l.V(jj)^k)-(obj.ciw^k))/((obj.rws^k)-(obj.ciw^k))) ;   
            elseif (obj.r_l.V(jj)>obj.rws) && (obj.r_l.V(jj)<=obj.cfw);
               Pwt(jj)=obj.Pwtnom;
            else
               Pwt(jj)=0;
            end
        end
        end
     end
      
   end
    
end