classdef bat
    
    
   properties 
    
      sigma
      nbatc
      nbatd
      Enom
      Emin
      Cch_nom
      cinv_bt
      cinv_btinv
      cmt_bt
      Ebtcc
      Abt


   end
   
   methods
      
       function obj = bat(sigma, nbatc, nbatd, Enom, Emin, Cch_nom, cinv_bt, cinv_btinv, cmt_bt, Ebtcc, Abt)
         obj.sigma = sigma;
         obj.nbatc = nbatc;
         obj.nbatd = nbatd;
         obj.Enom = Enom;
         obj.Emin = Emin;
         obj.Cch_nom = Cch_nom;
         obj.cinv_bt = cinv_bt;
         obj.cinv_btinv = cinv_btinv;
         obj.cmt_bt = cmt_bt;
         obj.Ebtcc = Ebtcc;
         obj.Abt = Abt;
       end
            
   end
    
end