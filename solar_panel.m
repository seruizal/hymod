classdef solar_panel
    
    
   properties 
      r_l
      Ppvnom
      miu_pmax
      Ga0 
      Pmax 
      NOCT 
      TM0 
      fpv 
      Nm 
      cinv_pv
      cinv_pvinv
      cmt_pv
      Epvcc
      Apv
      Ppv 

   end
   
   methods
      
       function obj = solar_panel(miu_pmax, Ga0, Pmax, NOCT, TM0, fpv, Nm, cinv_pv,...
         cinv_pvinv, cmt_pv, Epvcc, Apv, r_l)
         obj.Ppvnom = Pmax*Nm;
         obj.miu_pmax = miu_pmax;
         obj.Ga0 = Ga0*1e3;
         obj.Pmax = Pmax*1e3;
         obj.NOCT = NOCT;
         obj.TM0 = TM0;
         obj.fpv = fpv;
         obj.Nm = Nm;
         obj.cinv_pv = cinv_pv;
         obj.cinv_pvinv = cinv_pvinv;
         obj.cmt_pv = cmt_pv;
         obj.Epvcc = Epvcc;
         obj.Apv = Apv;
         obj.r_l = r_l;
         obj.Ppv = power_pv(obj);
       end
       
       function Ppv = power_pv(obj)
          flg2 = getappdata(0,'flg2');
          Ti = getappdata(0,'Ti');
          Tf = getappdata(0,'Tf');
          if flg2==0
             Ppv=zeros(Tf-Ti,1); 
          else
              
          TM=(obj.r_l.Tm+obj.r_l.G).*((obj.NOCT-20)/800)*(1-0.7182/0.9);
          Ppv=1e-3.*obj.Nm.*(obj.Pmax*obj.fpv)*(obj.r_l.G./obj.Ga0).*(1+(obj.miu_pmax).*(TM-obj.TM0));
          end
     end
      
   end
    
end