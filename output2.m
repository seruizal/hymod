function varargout = output2(varargin)
% OUTPUT2 MATLAB code for output2.fig
%      OUTPUT2, by itself, creates a new OUTPUT2 or raises the existing
%      singleton*.
%
%      H = OUTPUT2 returns the handle to a new OUTPUT2 or the handle to
%      the existing singleton*.
%
%      OUTPUT2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OUTPUT2.M with the given input arguments.
%
%      OUTPUT2('Property','Value',...) creates a new OUTPUT2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before output2_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to output2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help output2

% Last Modified by GUIDE v2.5 29-Aug-2017 10:10:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @output2_OpeningFcn, ...
                   'gui_OutputFcn',  @output2_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before output2 is made visible.
function output2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to output2 (see VARARGIN)

% Choose default command line output for output2
handles.output = hObject;

cd1=[]; %%current data vector of powers
cd2=[]; %%current data vector of energies
leg={}; %% legend text
setappdata(0,'cd1',cd1);
setappdata(0,'cd2',cd2);
setappdata(0,'leg',leg);
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes output2 wait for user response (see UIRESUME)
% uiwait(handles.GUI9);


% --- Outputs from this function are returned to the command line.
function varargout = output2_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO) 
Pload=getappdata(0,'Pload');
Pus=getappdata(0,'Pus');
Pdg=getappdata(0,'Pdg');
Psol=getappdata(0,'Psol');
Pwind=getappdata(0,'Pwind');
Pren=getappdata(0,'Pren');
Pbatc=getappdata(0,'Pbatc');
Pbatd=getappdata(0,'Pbatd');
Ebat=getappdata(0,'Ebat');
Phpb=getappdata(0,'Phpb');
Phpt=getappdata(0,'Phpt');
Qur=getappdata(0,'Qur');
Pdcev=getappdata(0,'Pdcev');
Pcev=getappdata(0,'Pcev');
Ebatev=getappdata(0,'Ebatev');
Pcevnd=getappdata(0,'Pcevnd');
Pcevt=getappdata(0,'Pcevt');
Pdcev2=getappdata(0,'Pdcev2');
Pdcev2g=getappdata(0,'Pdcev2g');


% Determine the selected data set.
str = get(hObject, 'String');
val = get(hObject,'Value');
set(gca,'Position', [12   4.5385  114.5900   21.7308]);
% Set current data to the selected data set.

switch str{val};
case 'Demand power'
    cla
    x1=1:size(Pload);
    cd1=getappdata(0,'cd1');
    cd2=getappdata(0,'cd2');
    leg=getappdata(0,'leg');
    cd1i=[cd1,Pload];
    leg=[leg str{val}];
    setappdata(0,'cd1',cd1i);
    setappdata(0,'leg',leg);
    cd1=getappdata(0,'cd1');
    leg=getappdata(0,'leg');
    if ~isempty(cd2)   
    [AX H1 H2] = plotyy([repmat(x1',1,1)],cd2,[repmat(x1',1,1)]',cd1','plot');
    ylabel(AX(1),'Energy [kWh]') % left y-axis
    ylabel(AX(2),'Power [kW]'); % right y-axis
    set(H1,'linewidth',1.5);
    set(H2,'linewidth',1.5);
    set(AX(1),'YColor','k');
    set(AX(2),'YColor','k');
    col=get(0,'DefaultAxesColorOrder');
    set(H1, {'Color'}, num2cell(col(1:size(cd1,2),:),2));
    set(H2, {'Color'}, num2cell(col(size(cd1,2)+1:size(cd1,2)+size(cd2,2),:),2));
    lh=legend([H1; H2],cellstr(leg));
    AX(2).ActivePositionProperty = 'position';
    set(AX(2),'OuterPosition',[-11.1880    0.6038  150   28.3481]);
    set(lh,'Position',[0.74   0.4575    0.1994    0.1498]);
    else
    plot(cd1,'linewidth',1.5);
    set(gca, 'XTickMode', 'auto', 'XTickLabelMode', 'auto');
    set(gca, 'YTickMode', 'auto', 'YTickLabelMode', 'auto');
    set(gca, 'PositionMode', 'auto');
    xlabel('time [hours]'); ylabel('Power [kW]');
    axis tight   % set tight range
    lg=legend(cellstr(leg));
    set(lg,'location','eastoutside');
    set(gca,'Position', [12   4.5385  114.5900   21.7308]);
     ylim([min(cd1(:)) max(cd1(:))]);
     set(lg,'Position',[0.74   0.4575    0.1994    0.1498]);
    end

case 'Power un-supplied'
    cla
    x1=1:size(Pus);
    cd1=getappdata(0,'cd1');
    cd2=getappdata(0,'cd2');
    leg=getappdata(0,'leg');
    cd1i=[cd1,Pus];
    leg=[leg str{val}];
    setappdata(0,'cd1',cd1i);
    setappdata(0,'leg',leg);
    cd1=getappdata(0,'cd1');
    leg=getappdata(0,'leg');
    if ~isempty(cd2)
    [AX H1 H2] = plotyy([repmat(x1',1,1)],cd2,[repmat(x1',1,1)]',cd1','plot');
    ylabel(AX(1),'Energy [kWh]') % left y-axis
    ylabel(AX(2),'Power [kW]'); % right y-axis
    set(H1,'linewidth',1.5);
    set(H2,'linewidth',1.5);
    set(AX(1),'YColor','k');
    set(AX(2),'YColor','k');
    col=get(0,'DefaultAxesColorOrder');
    set(H1, {'Color'}, num2cell(col(1:size(cd1,2),:),2));
    set(H2, {'Color'}, num2cell(col(size(cd1,2)+1:size(cd1,2)+size(cd2,2),:),2));
    lh=legend([H1; H2],cellstr(leg));
    AX(2).ActivePositionProperty = 'position';
    set(AX(2),'OuterPosition',[-11.1880    0.6038  150   28.3481]);
    set(lh,'Position',[0.74   0.4575    0.1994    0.1498]);
    else
    plot(cd1,'linewidth',1.5); hold on;
    set(gca, 'XTickMode', 'auto', 'XTickLabelMode', 'auto');
    set(gca, 'YTickMode', 'auto', 'YTickLabelMode', 'auto');
    set(gca, 'PositionMode', 'auto');
    xlabel('time [hours]'); ylabel('Power [kW]');
    lg=legend(cellstr(leg));
    set(lg,'location','eastoutside');
    set(gca,'Position', [12   4.5385  114.5900   21.7308]);
     ylim([min(cd1(:)) max(cd1(:))]);
     set(lg,'Position',[0.74   0.4575    0.1994    0.1498]);
    end
    
case 'Diesel generation' 
    cla
    x1=1:size(Pdg);
    cd1=getappdata(0,'cd1');
    cd2=getappdata(0,'cd2');
    leg=getappdata(0,'leg');
    cd1i=[cd1,Pdg];
    leg=[leg str{val}];
    setappdata(0,'cd1',cd1i);
    setappdata(0,'leg',leg);
    cd1=getappdata(0,'cd1');
    leg=getappdata(0,'leg');
    if ~isempty(cd2)
    [AX H1 H2] = plotyy([repmat(x1',1,1)],cd2,[repmat(x1',1,1)]',cd1','plot');
    ylabel(AX(1),'Energy [kWh]') % left y-axis
    ylabel(AX(2),'Power [kW]'); % right y-axis
    set(H1,'linewidth',1.5);
    set(H2,'linewidth',1.5);
    set(AX(1),'YColor','k');
    set(AX(2),'YColor','k');
    col=get(0,'DefaultAxesColorOrder');
    set(H1, {'Color'}, num2cell(col(1:size(cd1,2),:),2));
    set(H2, {'Color'}, num2cell(col(size(cd1,2)+1:size(cd1,2)+size(cd2,2),:),2));
    lh=legend([H1; H2],cellstr(leg));
    AX(2).ActivePositionProperty = 'position';
    set(AX(2),'OuterPosition',[-11.1880    0.6038  150   28.3481]);
    set(lh,'Position',[0.74   0.4575    0.1994    0.1498]);
    else
    plot(cd1,'linewidth',1.5); hold on;
    set(gca, 'XTickMode', 'auto', 'XTickLabelMode', 'auto');
    set(gca, 'YTickMode', 'auto', 'YTickLabelMode', 'auto');
    set(gca, 'PositionMode', 'auto');
    xlabel('time [hours]'); ylabel('Power [kW]');
    lg=legend(cellstr(leg));
    set(lg,'location','eastoutside');
    set(gca,'Position', [12   4.5385  114.5900   21.7308]);
     ylim([min(cd1(:)) max(cd1(:))]);
     set(lg,'Position',[0.74   0.4575    0.1994    0.1498]);
    end
case 'Solar generation' 
    cla
    x1=1:size(Psol);
    cd1=getappdata(0,'cd1');
    cd2=getappdata(0,'cd2');
    leg=getappdata(0,'leg');
    cd1i=[cd1,Psol];
    leg=[leg str{val}];
    setappdata(0,'cd1',cd1i);
    setappdata(0,'leg',leg);
    cd1=getappdata(0,'cd1');
    leg=getappdata(0,'leg');
    if ~isempty(cd2)
    [AX H1 H2] = plotyy([repmat(x1',1,1)],cd2,[repmat(x1',1,1)]',cd1','plot');
    ylabel(AX(1),'Energy [kWh]') % left y-axis
    ylabel(AX(2),'Power [kW]'); % right y-axis
    set(H1,'linewidth',1.5);
    set(H2,'linewidth',1.5);
    set(AX(1),'YColor','k');
    set(AX(2),'YColor','k');
    col=get(0,'DefaultAxesColorOrder');
    set(H1, {'Color'}, num2cell(col(1:size(cd1,2),:),2));
    set(H2, {'Color'}, num2cell(col(size(cd1,2)+1:size(cd1,2)+size(cd2,2),:),2));
    lh=legend([H1; H2],cellstr(leg));
    AX(2).ActivePositionProperty = 'position';
    set(AX(2),'OuterPosition',[-11.1880    0.6038  150   28.3481]);
    set(lh,'Position',[0.74   0.4575    0.1994    0.1498]);
    else
    plot(cd1,'linewidth',1.5); hold on;
    set(gca, 'XTickMode', 'auto', 'XTickLabelMode', 'auto');
    set(gca, 'YTickMode', 'auto', 'YTickLabelMode', 'auto');
    set(gca, 'PositionMode', 'auto');
    xlabel('time [hours]'); ylabel('Power [kW]');
    lg=legend(cellstr(leg));
    set(lg,'location','eastoutside');
     set(gca,'Position', [12   4.5385  114.5900   21.7308]);
     ylim([min(cd1(:)) max(cd1(:))]);
     set(lg,'Position',[0.74   0.4575    0.1994    0.1498]);
    end
case 'Wind generation' 
   cla
    x1=1:size(Pwind');
    cd1=getappdata(0,'cd1');
    cd2=getappdata(0,'cd2');
    leg=getappdata(0,'leg');
    cd1i=[cd1,Pwind'];
    leg=[leg str{val}];
    setappdata(0,'cd1',cd1i);
    setappdata(0,'leg',leg);
    cd1=getappdata(0,'cd1');
    leg=getappdata(0,'leg');
    if ~isempty(cd2)
    [AX H1 H2] = plotyy([repmat(x1',1,1)],cd2,[repmat(x1',1,1)]',cd1','plot');
    ylabel(AX(1),'Energy [kWh]') % left y-axis
    ylabel(AX(2),'Power [kW]'); % right y-axis
    set(H1,'linewidth',1.5);
    set(H2,'linewidth',1.5);
    set(AX(1),'YColor','k');
    set(AX(2),'YColor','k');
    col=get(0,'DefaultAxesColorOrder');
    set(H1, {'Color'}, num2cell(col(1:size(cd1,2),:),2));
    set(H2, {'Color'}, num2cell(col(size(cd1,2)+1:size(cd1,2)+size(cd2,2),:),2));
    lh=legend([H1; H2],cellstr(leg));
    AX(2).ActivePositionProperty = 'position';
    set(AX(2),'OuterPosition',[-11.1880    0.6038  150   28.3481]);
    set(lh,'Position',[0.74   0.4575    0.1994    0.1498]);
    else
    plot(cd1,'linewidth',1.5); hold on;
    set(gca, 'XTickMode', 'auto', 'XTickLabelMode', 'auto');
    set(gca, 'YTickMode', 'auto', 'YTickLabelMode', 'auto');
    set(gca, 'PositionMode', 'auto');
    xlabel('time [hours]'); ylabel('Power [kW]');
    lg=legend(cellstr(leg));
    set(lg,'location','eastoutside');
    set(gca,'Position', [12   4.5385  114.5900   21.7308]);
     ylim([min(cd1(:)) max(cd1(:))]);
     set(lg,'Position',[0.74   0.4575    0.1994    0.1498]);
    end
case 'Renewable generation' 
    cla
    x1=1:size(Pren);
    cd1=getappdata(0,'cd1');
    cd2=getappdata(0,'cd2');
    leg=getappdata(0,'leg');
    cd1i=[cd1,Pren];
    leg=[leg str{val}];
    setappdata(0,'cd1',cd1i);
    setappdata(0,'leg',leg);
    cd1=getappdata(0,'cd1');
    leg=getappdata(0,'leg');
    if ~isempty(cd2)
    [AX H1 H2] = plotyy([repmat(x1',1,1)],cd2,[repmat(x1',1,1)]',cd1','plot');
    ylabel(AX(1),'Energy [kWh]') % left y-axis
    ylabel(AX(2),'Power [kW]'); % right y-axis
    set(H1,'linewidth',1.5);
    set(H2,'linewidth',1.5);
    set(AX(1),'YColor','k');
    set(AX(2),'YColor','k');
    col=get(0,'DefaultAxesColorOrder');
    set(H1, {'Color'}, num2cell(col(1:size(cd1,2),:),2));
    set(H2, {'Color'}, num2cell(col(size(cd1,2)+1:size(cd1,2)+size(cd2,2),:),2));
    lh=legend([H1; H2],cellstr(leg));
    AX(2).ActivePositionProperty = 'position';
    set(AX(2),'OuterPosition',[-11.1880    0.6038  150   28.3481]);
    set(lh,'Position',[0.74   0.4575    0.1994    0.1498]);
    else
    plot(cd1,'linewidth',1.5); hold on;
    set(gca, 'XTickMode', 'auto', 'XTickLabelMode', 'auto');
    set(gca, 'YTickMode', 'auto', 'YTickLabelMode', 'auto');
    set(gca, 'PositionMode', 'auto');
    xlabel('time [hours]'); ylabel('Power [kW]');
    lg=legend(cellstr(leg));
    set(lg,'location','eastoutside');
    set(gca,'Position', [12   4.5385  114.5900   21.7308]);
     ylim([min(cd1(:)) max(cd1(:))]);
     set(lg,'Position',[0.74   0.4575    0.1994    0.1498]);
    end
case 'Batteries charging power'
    cla
    x1=1:size(Pbatc);
    cd1=getappdata(0,'cd1');
    cd2=getappdata(0,'cd2');
    leg=getappdata(0,'leg');
    cd1i=[cd1,Pbatc];
    leg=[leg str{val}];
    setappdata(0,'cd1',cd1i);
    setappdata(0,'leg',leg);
    cd1=getappdata(0,'cd1');
    leg=getappdata(0,'leg');
    if ~isempty(cd2)
    [AX H1 H2] = plotyy([repmat(x1',1,1)],cd2,[repmat(x1',1,1)]',cd1','plot');
    ylabel(AX(1),'Energy [kWh]') % left y-axis
    ylabel(AX(2),'Power [kW]'); % right y-axis
    set(H1,'linewidth',1.5);
    set(H2,'linewidth',1.5);
    set(AX(1),'YColor','k');
    set(AX(2),'YColor','k');
    col=get(0,'DefaultAxesColorOrder');
    set(H1, {'Color'}, num2cell(col(1:size(cd1,2),:),2));
    set(H2, {'Color'}, num2cell(col(size(cd1,2)+1:size(cd1,2)+size(cd2,2),:),2));
    lh=legend([H1; H2],cellstr(leg));
    AX(2).ActivePositionProperty = 'position';
    set(AX(2),'OuterPosition',[-11.1880    0.6038  150   28.3481]);
    set(lh,'Position',[0.74   0.4575    0.1994    0.1498]);
    else
    plot(cd1,'linewidth',1.5); hold on;
    set(gca, 'XTickMode', 'auto', 'XTickLabelMode', 'auto');
    set(gca, 'YTickMode', 'auto', 'YTickLabelMode', 'auto');
    set(gca, 'PositionMode', 'auto');
    xlabel('time [hours]'); ylabel('Power [kW]');
    lg=legend(cellstr(leg));
    set(lg,'location','eastoutside');
    set(gca,'Position', [12   4.5385  114.5900   21.7308]);
     ylim([min(cd1(:)) max(cd1(:))]);
     set(lg,'Position',[0.74   0.4575    0.1994    0.1498]);
    end
case 'Batteries discharging power' 
    cla
    x1=1:size(Pbatd);
    cd1=getappdata(0,'cd1');
    cd2=getappdata(0,'cd2');
    leg=getappdata(0,'leg');
    cd1i=[cd1,Pbatd];
    leg=[leg str{val}];
    setappdata(0,'cd1',cd1i);
    setappdata(0,'leg',leg);
    cd1=getappdata(0,'cd1');
    leg=getappdata(0,'leg');
    if ~isempty(cd2)
    [AX H1 H2] = plotyy([repmat(x1',1,1)],cd2,[repmat(x1',1,1)]',cd1','plot');
    ylabel(AX(1),'Energy [kWh]') % left y-axis
    ylabel(AX(2),'Power [kW]'); % right y-axis
    set(H1,'linewidth',1.5);
    set(H2,'linewidth',1.5);
    set(AX(1),'YColor','k');
    set(AX(2),'YColor','k');
    col=get(0,'DefaultAxesColorOrder');
    set(H1, {'Color'}, num2cell(col(1:size(cd1,2),:),2));
    set(H2, {'Color'}, num2cell(col(size(cd1,2)+1:size(cd1,2)+size(cd2,2),:),2));
    lh=legend([H1; H2],cellstr(leg));
    AX(2).ActivePositionProperty = 'position';
    set(AX(2),'OuterPosition',[-11.1880    0.6038  150   28.3481]);
    set(lh,'Position',[0.74   0.4575    0.1994    0.1498]);
    else
    plot(cd1,'linewidth',1.5); hold on;
    set(gca, 'XTickMode', 'auto', 'XTickLabelMode', 'auto');
    set(gca, 'YTickMode', 'auto', 'YTickLabelMode', 'auto');
    set(gca, 'PositionMode', 'auto');
    xlabel('time [hours]'); ylabel('Power [kW]');
    lg=legend(cellstr(leg));
    set(lg,'location','eastoutside');
    set(gca,'Position', [12   4.5385  114.5900   21.7308]);
     ylim([min(cd1(:)) max(cd1(:))]);
     set(lg,'Position',[0.74   0.4575    0.1994    0.1498]);
    end
case 'Energy stored in batteries'
    cla
    delete(allchild(handles.axes1));
    cd1=getappdata(0,'cd1');
    cd2=getappdata(0,'cd2');
    leg=getappdata(0,'leg');
    Ebat1=Ebat(1:end-1);
    x1=1:size(Ebat1);
    cd2i=[cd2,Ebat1];
    leg=[leg str{val}];
    setappdata(0,'cd2',cd2i);
    setappdata(0,'leg',leg);
    cd2=getappdata(0,'cd2');
    leg=getappdata(0,'leg');
    if ~isempty(cd1)
    [AX H1 H2] = plotyy([repmat(x1',1,1)],cd1,[repmat(x1',1,1)]',cd2','plot');
    ylabel(AX(1),'Power [kW]') % left y-axis
    ylabel(AX(2),'Energy [kWh]'); % right y-axis
    set(H1,'linewidth',1.5);
    set(H2,'linewidth',1.5);
    set(AX(1),'YColor','k');
    set(AX(2),'YColor','k');
    col=get(0,'DefaultAxesColorOrder');
    set(H1, {'Color'}, num2cell(col(1:size(cd1,2),:),2));
    set(H2, {'Color'}, num2cell(col(size(cd1,2)+1:size(cd1,2)+size(cd2,2),:),2));
    lh=legend([H1; H2],cellstr(leg));
    AX(2).ActivePositionProperty = 'position';
    set(AX(2),'OuterPosition',[-11.1880    0.6038  150   28.3481]);
    set(lh,'Position',[0.74   0.4575    0.1994    0.1498]);
    
    else
     plot(cd2,'linewidth',1.5); hold on;
     xlabel('time [hours]'); ylabel('Energy [kWh]');
     set(gca, 'XTickMode', 'auto', 'XTickLabelMode', 'auto');
     set(gca, 'YTickMode', 'auto', 'YTickLabelMode', 'auto');
     lg=legend(cellstr(leg));
     set(lg,'location','eastoutside');  
     set(gca,'Position', [12   4.5385  114.5900   21.7308]);
     ylim([min(cd2(:)) max(cd2(:))]);
     set(lg,'Position',[0.74   0.4575    0.1994    0.1498]);
    end
    
case 'Water pump power' 
    cla
    x1=1:size(Phpb);
    cd1=getappdata(0,'cd1');
    cd2=getappdata(0,'cd2');
    leg=getappdata(0,'leg');
    cd1i=[cd1,Phpb];
    leg=[leg str{val}];
    setappdata(0,'cd1',cd1i);
    setappdata(0,'leg',leg);
    cd1=getappdata(0,'cd1');
    leg=getappdata(0,'leg');
    if ~isempty(cd2)
    [AX H1 H2] = plotyy([repmat(x1',1,1)],cd2,[repmat(x1',1,1)]',cd1','plot');
    ylabel(AX(1),'Energy [kWh]') % left y-axis
    ylabel(AX(2),'Power [kW]'); % right y-axis
    set(H1,'linewidth',1.5);
    set(H2,'linewidth',1.5);
    set(AX(1),'YColor','k');
    set(AX(2),'YColor','k');
    col=get(0,'DefaultAxesColorOrder');
    set(H1, {'Color'}, num2cell(col(1:size(cd1,2),:),2));
    set(H2, {'Color'}, num2cell(col(size(cd1,2)+1:size(cd1,2)+size(cd2,2),:),2));
    lh=legend([H1; H2],cellstr(leg));
    AX(2).ActivePositionProperty = 'position';
    set(AX(2),'OuterPosition',[-11.1880    0.6038  150   28.3481]);
    set(lh,'Position',[0.74   0.4575    0.1994    0.1498]);
    else
    plot(cd1,'linewidth',1.5); hold on;
    set(gca, 'XTickMode', 'auto', 'XTickLabelMode', 'auto');
    set(gca, 'YTickMode', 'auto', 'YTickLabelMode', 'auto');
    set(gca, 'PositionMode', 'auto');
    xlabel('time [hours]'); ylabel('Power [kW]');
    lg=legend(cellstr(leg));
    set(lg,'location','eastoutside');
    set(gca,'Position', [12   4.5385  114.5900   21.7308]);
     ylim([min(cd1(:)) max(cd1(:))]);
     set(lg,'Position',[0.74   0.4575    0.1994    0.1498]);
    end
case 'Hydraulic turbine generation' 
    cla
    x1=1:size(Phpt);
    cd1=getappdata(0,'cd1');
    cd2=getappdata(0,'cd2');
    leg=getappdata(0,'leg');
    cd1i=[cd1,Phpt];
    leg=[leg str{val}];
    setappdata(0,'cd1',cd1i);
    setappdata(0,'leg',leg);
    cd1=getappdata(0,'cd1');
    leg=getappdata(0,'leg');
    if ~isempty(cd2)
    [AX H1 H2] = plotyy([repmat(x1',1,1)],cd2,[repmat(x1',1,1)]',cd1','plot');
    ylabel(AX(1),'Energy [kWh]') % left y-axis
    ylabel(AX(2),'Power [kW]'); % right y-axis
    set(H1,'linewidth',1.5);
    set(H2,'linewidth',1.5);
    set(AX(1),'YColor','k');
    set(AX(2),'YColor','k');
    col=get(0,'DefaultAxesColorOrder');
    set(H1, {'Color'}, num2cell(col(1:size(cd1,2),:),2));
    set(H2, {'Color'}, num2cell(col(size(cd1,2)+1:size(cd1,2)+size(cd2,2),:),2));
    lh=legend([H1; H2],cellstr(leg));
    AX(2).ActivePositionProperty = 'position';
    set(AX(2),'OuterPosition',[-11.1880    0.6038  150   28.3481]);
    set(lh,'Position',[0.74   0.4575    0.1994    0.1498]);
    else
    plot(cd1,'linewidth',1.5); hold on;
    set(gca, 'XTickMode', 'auto', 'XTickLabelMode', 'auto');
    set(gca, 'YTickMode', 'auto', 'YTickLabelMode', 'auto');
    set(gca, 'PositionMode', 'auto');
    xlabel('time [hours]'); ylabel('Power [kW]');
    lg=legend(cellstr(leg));
    set(lg,'location','eastoutside');
    set(gca,'Position', [12   4.5385  114.5900   21.7308]);
     ylim([min(cd1(:)) max(cd1(:))]);
     set(lg,'Position',[0.74   0.4575    0.1994    0.1498]);
    end
case 'Energy stored in water tank' 
    cla
    delete(allchild(handles.axes1));
    cd1=getappdata(0,'cd1');
    cd2=getappdata(0,'cd2');
    leg=getappdata(0,'leg');
    Qur1=Qur(1:end-1);
    x1=1:size(Qur1);
    cd2i=[cd2,Qur1];
    leg=[leg str{val}];
    setappdata(0,'cd2',cd2i);
    setappdata(0,'leg',leg);
    cd2=getappdata(0,'cd2');
    leg=getappdata(0,'leg');
    if ~isempty(cd1)
    [AX H1 H2] = plotyy([repmat(x1',1,1)],cd1,[repmat(x1',1,1)]',cd2','plot');
    ylabel(AX(1),'Power [kW]') % left y-axis
    ylabel(AX(2),'Energy [kWh]'); % right y-axis
    set(H1,'linewidth',1.5);
    set(H2,'linewidth',1.5);
    set(AX(1),'YColor','k');
    set(AX(2),'YColor','k');
    col=get(0,'DefaultAxesColorOrder');
    set(H1, {'Color'}, num2cell(col(1:size(cd1,2),:),2));
    set(H2, {'Color'}, num2cell(col(size(cd1,2)+1:size(cd1,2)+size(cd2,2),:),2));
    lh=legend([H1; H2],cellstr(leg));
    AX(2).ActivePositionProperty = 'position';
    set(AX(2),'OuterPosition',[-11.1880    0.6038  150   28.3481]);
    set(lh,'Position',[0.74   0.4575    0.1994    0.1498]);
    
    else
     plot(cd2,'linewidth',1.5); hold on;
     xlabel('time [hours]'); ylabel('Energy [kWh]');
     set(gca, 'XTickMode', 'auto', 'XTickLabelMode', 'auto');
     set(gca, 'YTickMode', 'auto', 'YTickLabelMode', 'auto');
     lg=legend(cellstr(leg));
     set(lg,'location','eastoutside');  
     set(gca,'Position', [12    4.5385  114.5900   21.7308]);
     ylim([min(cd2(:)) max(cd2(:))]);
     set(lg,'Position',[0.74   0.4575    0.1994    0.1498]);
    end
    
case 'Charging power of EVs batteries' 
    cla
    x1=1:size(Pcevt);
    cd1=getappdata(0,'cd1');
    cd2=getappdata(0,'cd2');
    leg=getappdata(0,'leg');
    cd1i=[cd1,Pcevt];
    leg=[leg str{val}];
    setappdata(0,'cd1',cd1i);
    setappdata(0,'leg',leg);
    cd1=getappdata(0,'cd1');
    leg=getappdata(0,'leg');
    if ~isempty(cd2)
    [AX H1 H2] = plotyy([repmat(x1',1,1)],cd2,[repmat(x1',1,1)]',cd1','plot');
    ylabel(AX(1),'Energy [kWh]') % left y-axis
    ylabel(AX(2),'Power [kW]'); % right y-axis
    set(H1,'linewidth',1.5);
    set(H2,'linewidth',1.5);
    set(AX(1),'YColor','k');
    set(AX(2),'YColor','k');
    col=get(0,'DefaultAxesColorOrder');
    set(H1, {'Color'}, num2cell(col(1:size(cd1,2),:),2));
    set(H2, {'Color'}, num2cell(col(size(cd1,2)+1:size(cd1,2)+size(cd2,2),:),2));
    lh=legend([H1; H2],cellstr(leg));
    AX(2).ActivePositionProperty = 'position';
    set(AX(2),'OuterPosition',[-11.1880    0.6038  150   28.3481]);
    set(lh,'Position',[0.74   0.4575    0.1994    0.1498]);
    else
    plot(cd1,'linewidth',1.5); hold on;
    set(gca, 'XTickMode', 'auto', 'XTickLabelMode', 'auto');
    set(gca, 'YTickMode', 'auto', 'YTickLabelMode', 'auto');
    set(gca, 'PositionMode', 'auto');
    xlabel('time [hours]'); ylabel('Power [kW]');
    lg=legend(cellstr(leg));
    set(lg,'location','eastoutside');
    set(gca,'Position', [12    4.5385  114.5900   21.7308]);
    ylim([min(cd1(:)) max(cd1(:))]);
    set(lg,'Position',[0.74   0.4575    0.1994    0.1498]);
    end
 case 'Charging power of EVs type 1 batteries'
    cla
    x1=1:size(Pcevnd);
    cd1=getappdata(0,'cd1');
    cd2=getappdata(0,'cd2');
    leg=getappdata(0,'leg');
    cd1i=[cd1,Pcevnd];
    leg=[leg str{val}];
    setappdata(0,'cd1',cd1i);
    setappdata(0,'leg',leg);
    cd1=getappdata(0,'cd1');
    leg=getappdata(0,'leg');
    if ~isempty(cd2)
    [AX H1 H2] = plotyy([repmat(x1',1,1)],cd2,[repmat(x1',1,1)]',cd1','plot');
    ylabel(AX(1),'Energy [kWh]') % left y-axis
    ylabel(AX(2),'Power [kW]'); % right y-axis
    set(H1,'linewidth',1.5);
    set(H2,'linewidth',1.5);
    set(AX(1),'YColor','k');
    set(AX(2),'YColor','k');
    col=get(0,'DefaultAxesColorOrder');
    set(H1, {'Color'}, num2cell(col(1:size(cd1,2),:),2));
    set(H2, {'Color'}, num2cell(col(size(cd1,2)+1:size(cd1,2)+size(cd2,2),:),2));
    lh=legend([H1; H2],cellstr(leg));
    AX(2).ActivePositionProperty = 'position';
    set(AX(2),'OuterPosition',[-11.1880    0.6038  150   28.3481]);
    set(lh,'Position',[0.74   0.4575    0.1994    0.1498]);
    else
    plot(cd1,'linewidth',1.5); hold on;
    set(gca, 'XTickMode', 'auto', 'XTickLabelMode', 'auto');
    set(gca, 'YTickMode', 'auto', 'YTickLabelMode', 'auto');
    set(gca, 'PositionMode', 'auto');
    xlabel('time [hours]'); ylabel('Power [kW]');
    lg=legend(cellstr(leg));
    set(lg,'location','eastoutside');
     set(gca,'Position', [12   4.5385  114.5900   21.7308]);
     ylim([min(cd1(:)) max(cd1(:))]);
     set(lg,'Position',[0.74   0.4575    0.1994    0.1498]);
    end
case 'Charging power EVs type 2 batteries' 
    cla
    x1=1:size(Pcev);
    cd1=getappdata(0,'cd1');
    cd2=getappdata(0,'cd2');
    leg=getappdata(0,'leg');
    cd1i=[cd1,Pcev];
    leg=[leg str{val}];
    setappdata(0,'cd1',cd1i);
    setappdata(0,'leg',leg);
    cd1=getappdata(0,'cd1');
    leg=getappdata(0,'leg');
    if ~isempty(cd2)
    [AX H1 H2] = plotyy([repmat(x1',1,1)],cd2,[repmat(x1',1,1)]',cd1','plot');
    ylabel(AX(1),'Energy [kWh]') % left y-axis
    ylabel(AX(2),'Power [kW]'); % right y-axis
    set(H1,'linewidth',1.5);
    set(H2,'linewidth',1.5);
    set(AX(1),'YColor','k');
    set(AX(2),'YColor','k');
    col=get(0,'DefaultAxesColorOrder');
    set(H1, {'Color'}, num2cell(col(1:size(cd1,2),:),2));
    set(H2, {'Color'}, num2cell(col(size(cd1,2)+1:size(cd1,2)+size(cd2,2),:),2));
    lh=legend([H1; H2],cellstr(leg));
    AX(2).ActivePositionProperty = 'position';
    set(AX(2),'OuterPosition',[-11.1880    0.6038  150   28.3481]);
    set(lh,'Position',[0.74   0.4575    0.1994    0.1498]);
    else
    plot(cd1,'linewidth',1.5); hold on;
    set(gca, 'XTickMode', 'auto', 'XTickLabelMode', 'auto');
    set(gca, 'YTickMode', 'auto', 'YTickLabelMode', 'auto');
    set(gca, 'PositionMode', 'auto');
    xlabel('time [hours]'); ylabel('Power [kW]');
    lg=legend(cellstr(leg));
    set(lg,'location','eastoutside');
    set(gca,'Position', [12   4.5385  114.5900   21.7308]);
     ylim([min(cd1(:)) max(cd1(:))]);
     set(lg,'Position',[0.74   0.4575    0.1994    0.1498]);
    end
   
case 'Power discharged by EVs type 2 to the grid' 
    cla
    x1=1:size(Pdcev2g);
    cd1=getappdata(0,'cd1');
    cd2=getappdata(0,'cd2');
    leg=getappdata(0,'leg');
    cd1i=[cd1,Pdcev2g];
    leg=[leg str{val}];
    setappdata(0,'cd1',cd1i);
    setappdata(0,'leg',leg);
    cd1=getappdata(0,'cd1');
    leg=getappdata(0,'leg');
    if ~isempty(cd2)
    [AX H1 H2] = plotyy([repmat(x1',1,1)],cd2,[repmat(x1',1,1)]',cd1','plot');
    ylabel(AX(1),'Energy [kWh]') % left y-axis
    ylabel(AX(2),'Power [kW]'); % right y-axis
    set(H1,'linewidth',1.5);
    set(H2,'linewidth',1.5);
    set(AX(1),'YColor','k');
    set(AX(2),'YColor','k');
    col=get(0,'DefaultAxesColorOrder');
    set(H1, {'Color'}, num2cell(col(1:size(cd1,2),:),2));
    set(H2, {'Color'}, num2cell(col(size(cd1,2)+1:size(cd1,2)+size(cd2,2),:),2));
    lh=legend([H1; H2],cellstr(leg));
    AX(2).ActivePositionProperty = 'position';
    set(AX(2),'OuterPosition',[-11.1880    0.6038  140   28.3481]);
    set(lh,'Position',[0.74   0.4575    0.1994    0.1498]);
    else
    plot(cd1,'linewidth',1.5); hold on;
    set(gca, 'XTickMode', 'auto', 'XTickLabelMode', 'auto');
    set(gca, 'YTickMode', 'auto', 'YTickLabelMode', 'auto');
    set(gca, 'PositionMode', 'auto');
    xlabel('time [hours]'); ylabel('Power [kW]');
    lg=legend(cellstr(leg));
    set(lg,'location','eastoutside');
    set(gca,'Position', [12   4.5385  114.5900   21.7308]);
     ylim([min(cd1(:)) max(cd1(:))]);
     set(lg,'Position',[0.74   0.4575    0.1994    0.1498]);
    end
case 'Discharging driving power of EVs type 2' 
    cla
    x1=1:size(Pdcev2);
    cd1=getappdata(0,'cd1');
    cd2=getappdata(0,'cd2');
    leg=getappdata(0,'leg');
    cd1i=[cd1,Pdcev2];
    leg=[leg str{val}];
    setappdata(0,'cd1',cd1i);
    setappdata(0,'leg',leg);
    cd1=getappdata(0,'cd1');
    leg=getappdata(0,'leg');
    if ~isempty(cd2)
    [AX H1 H2] = plotyy([repmat(x1',1,1)],cd2,[repmat(x1',1,1)]',cd1','plot');
    ylabel(AX(1),'Energy [kWh]') % left y-axis
    ylabel(AX(2),'Power [kW]'); % right y-axis
    set(H1,'linewidth',1.5);
    set(H2,'linewidth',1.5);
    set(AX(1),'YColor','k');
    set(AX(2),'YColor','k');
    col=get(0,'DefaultAxesColorOrder');
    set(H1, {'Color'}, num2cell(col(1:size(cd1,2),:),2));
    set(H2, {'Color'}, num2cell(col(size(cd1,2)+1:size(cd1,2)+size(cd2,2),:),2));
    lh=legend([H1; H2],cellstr(leg));
    AX(2).ActivePositionProperty = 'position';
    set(AX(2),'OuterPosition',[-11.1880    0.6038  140   28.3481]);
    set(lh,'Position',[0.74   0.4575    0.1994    0.1498]);
    
    else
    plot(cd1,'linewidth',1.5); hold on;
    set(gca, 'XTickMode', 'auto', 'XTickLabelMode', 'auto');
    set(gca, 'YTickMode', 'auto', 'YTickLabelMode', 'auto');
    set(gca, 'PositionMode', 'auto');
    xlabel('time [hours]'); ylabel('Power [kW]');
    lg=legend(cellstr(leg));
    set(lg,'location','eastoutside');
    set(gca,'Position', [12    4.5385  114.5900   21.7308]);
    ylim([min(cd1(:)) max(cd1(:))]);
    set(lg,'Position',[0.74   0.4575    0.1994    0.1498]);
    end
case 'Energy in batteries of EVs type 2' 
    cla
    delete(allchild(handles.axes1));
    cd1=getappdata(0,'cd1');
    cd2=getappdata(0,'cd2');
    leg=getappdata(0,'leg');
    Ebatev1=Ebatev(1:end-1);
    x1=1:size(Ebatev1);
    cd2i=[cd2,Ebatev1];
    leg=[leg str{val}];
    setappdata(0,'cd2',cd2i);
    setappdata(0,'leg',leg);
    cd2=getappdata(0,'cd2');
    leg=getappdata(0,'leg');
    if ~isempty(cd1)
    [AX H1 H2] = plotyy([repmat(x1',1,1)],cd1,[repmat(x1',1,1)]',cd2','plot');
    ylabel(AX(1),'Power [kW]') % left y-axis
    ylabel(AX(2),'Energy [kWh]'); % right y-axis
    set(H1,'linewidth',1.5);
    set(H2,'linewidth',1.5);
    set(AX(1),'YColor','k');
    set(AX(2),'YColor','k');
    col=get(0,'DefaultAxesColorOrder');
    set(H1, {'Color'}, num2cell(col(1:size(cd1,2),:),2));
    set(H2, {'Color'}, num2cell(col(size(cd1,2)+1:size(cd1,2)+size(cd2,2),:),2));
    lh=legend([H1; H2],cellstr(leg));
    AX(2).ActivePositionProperty = 'position';
    set(AX(2),'OuterPosition',[-11.1880    0.6038  145   28.3481]);
    set(lh,'Position',[0.74   0.4575    0.1994    0.1498]);
    
    else
     plot(cd2,'linewidth',1.5); hold on;
     xlabel('time [hours]'); ylabel('Energy [kWh]');
     set(gca, 'XTickMode', 'auto', 'XTickLabelMode', 'auto');
     set(gca, 'YTickMode', 'auto', 'YTickLabelMode', 'auto');
     lg=legend(cellstr(leg));
     set(lg,'location','eastoutside');  
     set(gca,'Position', [12   4.5385  114.5900   21.7308]);
     ylim([min(cd2(:)) max(cd2(:))]);
     set(lg,'Position',[0.74   0.4575    0.1994    0.1498]);
    end
end


%  set(lh,'FontSize',10);
%  

%     legend(lh.String{:})
   % Save the handles structure.
guidata(hObject,handles)

% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(handles.GUI9)


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ax1 = findall(0, 'type', 'axes');
cla(handles.axes1); % Do a complete and total reset of the axes.
delete(allchild(handles.axes1));
% set(gca,'Position', [25.2280    4.5385  114.5900   21.7308]);
set(gca,'xtick',[]);
set(gca,'ytick',[]);
set(gca,'xlabel',[]);
set(gca,'ylabel',[]);
clear cd1 cd2 leg;
setappdata(0,'cd1',[]);
setappdata(0,'cd2',[]);
setappdata(0,'leg',{});

legend('off');
