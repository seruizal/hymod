  function Cs=cost(xn,x,obj)
         
        Nwt = xn(1);
        Npv = xn(2);
        Nbt = xn(3);
        Vol = xn(4);
        
        
        Pus = x(1:obj.r_l.N);
        Pdg = x(obj.r_l.N+1:2*obj.r_l.N);
        Pbatc1 = x(2*obj.r_l.N+1:3*obj.r_l.N);
        Pbatd1 = x(3*obj.r_l.N+1:4*obj.r_l.N);
        Phpb1 = x(4*obj.r_l.N+1:5*obj.r_l.N);
        Phpt1 = x(5*obj.r_l.N+1:6*obj.r_l.N);
        Pdcev = x(6*obj.r_l.N+1:7*obj.r_l.N);
        Pcev = x(7*obj.r_l.N+1:8*obj.r_l.N);
        Pcevnd = x(8*obj.r_l.N+1:9*obj.r_l.N);

          %% Costs
        
          %% Diesel costs
          
          %%Unguia Diesel generators : 2 of 745 kVA
          %%Dcg=Cop+Cmant
          %%Fuel consumption for Diesel plants [ltr/kWh]: 0.2461 Pdg+0.084151 Pn_dg 
          Con_com=(1/3.78541).*((0.2461.*Pdg)+(0.084151*obj.dg.Ndg*obj.dg.Pdgnom.*ones(obj.r_l.Tf-obj.r_l.Ti,1))); %% Diesel fuel consumption [gal]
          cost_c=obj.dg.cost_c(obj.r_l.Ti+1:obj.r_l.Tf)*(8760/(obj.r_l.Tf-obj.r_l.Ti));%% Levelized cost for the calculation period
          Ccomb=cost_c'*Con_com;
          %%Lubricant consumption:0.001226 gal/kWh
          % % Dcg=Cop+Cmant
          Con_lub=0.001226.*Pdg; %% lubricant consumption
          cost_lub=obj.dg.cost_l*ones(obj.r_l.Tf-obj.r_l.Ti,1)*(8760/(obj.r_l.Tf-obj.r_l.Ti)); %% lubricant cost for Ungu�a
          Club=cost_lub'*Con_lub;
          %%Diesel operative costs
          Cop=Ccomb+Club;
          %%Maintenance costs
          Cmant_dg=obj.dg.cmt_dg*obj.dg.Ndg;
          Dcg=Cop+Cmant_dg;

          %% Photovoltaic panels costs
          %%Cpv=CI+Cinv+AOM
          %%Yearly costs 
          CI_pv11=obj.pv.cinv_pv*Npv; %%COP initial investment cost
          %%Power inverter cost
          CI_inv_pv1=Npv*obj.pv.cinv_pvinv; %%COP
          CI_inv_pv=CI_inv_pv1*(1+(1/(obj.prj_p.in+1)^10)); %%Inverters replacement
          CI_pv1=CI_pv11+CI_inv_pv1+CI_inv_pv;
          %% Yearly cost
          CI_pv=((obj.prj_p.in*(obj.prj_p.in+1)^obj.prj_p.n)/(-1+(obj.prj_p.in+1)^obj.prj_p.n))*CI_pv1;
          %%Operation and maintenance costs = 2% of the initial investment cost
          AOM_pv=obj.pv.cmt_pv*CI_pv;
          Cpv=CI_pv+AOM_pv;


          %% Wind turbines costs
          %%Cwt=CI+Ctow+Cinv+AOM
          %%Wind turbines initial investment cost + tower cost with a height of 18 m
          CI_wt11=Nwt*obj.wt.cinv_wt;
          %%Power inverter cost
          CI_inv_wt1=Nwt*obj.wt.cinv_wtinv;
          %CI_inv_wt=CI_inv_wt1*(1+(1/(in+1)^10)); %%Inverters replacement
          %% Fundations cost
          Cf_wt=obj.wt.cinv_fund*Nwt*obj.wt.Pwtnom;
          CI_wt1=CI_wt11+CI_inv_wt1+Cf_wt;
          %% Yearly cost
          CI_wt=((obj.prj_p.in*(obj.prj_p.in+1)^obj.prj_p.n)/(-1+(obj.prj_p.in+1)^obj.prj_p.n))*CI_wt1;
          %%Operation and maintenance costs = 2% of the initial investment cost
          AOM_wt=obj.wt.cmt_wt*CI_wt;
          Cwt=CI_wt+AOM_wt;


          %% Batteries costs
          %%Cb=CI+AOM
          %%Initial investment cost
          CI_bat11=Nbt*obj.bt.cinv_bt;
          %%Batteries replacement costs
          CI_bat12=CI_bat11*(1+(1/(obj.prj_p.in+1)^5)+(1/(obj.prj_p.in+1)^10)+(1/(obj.prj_p.in+1)^15));
          %% Bidirectional power converter costs
          CI_bat_conv11=obj.bt.cinv_btinv*Nbt;%%COP
          CI_bat_conv1=CI_bat_conv11*(1+(1/(obj.prj_p.in+1)^10));  %%Inverters replacement
          %%Yearly costs
          CI_bat1= CI_bat11+CI_bat12+CI_bat_conv11+CI_bat_conv1;
          CI_batt=((obj.prj_p.in*(obj.prj_p.in+1)^obj.prj_p.n)/(-1+(obj.prj_p.in+1)^obj.prj_p.n))*CI_bat1;
          %%Operation and maintenance costs = 2% of the initial investment cost
          AOM_bat=obj.bt.cmt_bt*CI_batt;
          Cbat=CI_batt+AOM_bat;


          %% Hydro- pumped storage system costs
          CI_hp=(obj.hp.cinv_p*max(Phpb1))+(obj.hp.cinv_t*max(Phpt1));%%COP
          CI_tank=obj.hp.cinv_V*Vol;%%COP
          % CI_pipe_water =(43.8739003*15845.65)*863; %%COP 12" pipe water with 863 m of longitude
          Chp1=(CI_hp+CI_tank);%+CI_pipe_water);
          Chp11=(((obj.prj_p.in*(obj.prj_p.in+1)^obj.prj_p.n)/(-1+(obj.prj_p.in+1)^obj.prj_p.n))*Chp1);
          %%Operation and maintenance costs = 4% of the initial investment cost
          HP_AOM=obj.hp.cmt_hp*Chp11;
          Chp=Chp11+HP_AOM;


          %% electric vehicles costs
          CI_ev=obj.ev.Nev*obj.ev.cinv_ev; %% Initial investment cost
          Cev=(((obj.prj_p.in*(obj.prj_p.in+1)^obj.prj_p.n)/(-1+(obj.prj_p.in+1)^obj.prj_p.n))*CI_ev);

          
          Ccs=sum(Dcg)+sum(Cpv)+sum(Cwt)+sum(Cbat)+sum(Chp)+sum(Cev);

          %% Costs of land usage
          Sbat=obj.bt.Abt*Nbt; %% Land usage of batteries
          Spv= obj.pv.Apv*obj.pv.Ppvnom*Npv;%% Land usage of photovoltaic panels
          Swt= obj.wt.Awt*Nwt; % %% Land usage of wind turbines
          ter1=obj.prj_p.ips*(Sbat+Spv+Swt);
          %% Yearly cost
          ter=((obj.prj_p.in*(obj.prj_p.in+1)^obj.prj_p.n)/(-1+(obj.prj_p.in+1)^obj.prj_p.n))*ter1;

          %% Total cost
          Cs=sum(Ccs)+sum(ter);
          

       end
       