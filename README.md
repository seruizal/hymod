# Hymod #

Hymod is Matlab tool for designing hybrid isolated microgrids, developed by researchers of Unversidad Nacional de Colombia

### What is this repository for? ###

* Hymod software allows to the users sizing hybrid isolated microgrids composed by a set of the next elements: photovoltaic panels, wind turbines, Diesel generators, batteries, hydro-pumped storage system, residential loads and electric vehicles. The goal of the design is minimize the annual cost and CO2 system emission.
* Hymod version 17.9.1
* Hymod version 17.9.2


### How do I get set up? ###

* To operate, Hymod requires the previous installation of software Matlab R2015b or later versions, CVX  and Gurobi.
* Download and save the folder of Hymod in any directory, and add the path of its location to Matlab. Next, open the file called micr.m in Matlab and run it. Then the next window will be open.
