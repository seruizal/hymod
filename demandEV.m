classdef demandEV
    
    
   properties 
        
%% Load data
      hsi %% EV2s type 2 working hours
      hpi %% EV2s type 2 working hours
      rev2 %% Daily driving distances of EVs type 2 [km] (it is assumed that is the same for all vehicles)
      lEV2 %% electric comsumption of EV [W/km]
      N %% Number of steps in the simulation period
      nd
      nfsv
%      %% hourly vehicle demand
%      
      Pev2d
      Ma
      Mb
      Mc
      Md
      
   end
   
   methods
      
      function obj = demandEV(hsi, hpi, rev2, lEV2, r_l)
         obj.hsi = hsi;
         obj.hpi = hpi;
         obj.rev2 = rev2;
         obj.lEV2 = lEV2;
         obj.N = r_l.N;
         [obj.Pev2d,obj.Ma,obj.Mb,obj.Mc,obj.Md,obj.nd,obj.nfsv] = ev_consumption(obj);
       end
     
       function [Pev2d,Ma,Mb,Mc,Md,nd,nfsv]=ev_consumption(obj)
         
          flg6 = getappdata(0,'flg6');
          Ti = getappdata(0,'Ti');
          Tf = getappdata(0,'Tf');
          
        if flg6==0
            rev2t=zeros(Tf-Ti,1);
            cEV2=zeros(Tf-Ti,1); %% Renault twizy consumption
        else
         for kk=1:size(obj.rev2,1)
          
          rev2t(kk)= sum(obj.rev2(kk,:));  
         
         end
         cEV2=obj.lEV2*rev2t*1e-3; %% Renault twizy consumption
        end
         
         
         
         
         ht=1:24; %% Total day hours vector
         hnsi=ht;
         hnsi(obj.hsi)=[];%% Non-working hours of type 2 EVs
         mbv=zeros(1,24);
         mbv(obj.hsi)=1; %% vector mb
         mb=diag(mbv);
         mdv=zeros(1,24);
         mdv(hnsi)=1; %% vector md
         md=diag(mdv);
         mcv=zeros(1,24);
         mcv(obj.hpi)=1; %% vector mc
         mc=diag(mcv);
         Mb=[];
         Mc=[];
         Md=[];
         nd=floor(obj.N/24); %% Days number at design period
         nfs=floor(nd/7); %% Numero de fines de semana
         nfsv=[];
          for jj=1:nfs
            nfsv=[nfsv;7*jj-1;7*jj];
          end

        nfsv=nfsv-3;

      for kk=1:nd
            if ~isempty(find(kk==nfsv))
             Mb=blkdiag(Mb,zeros(24,24));
             Md=blkdiag(Md,eye(24,24));
             Mc=blkdiag(Mc,mc);
            else
             Mb=blkdiag(Mb,mb);
             Mc=blkdiag(Mc,mc);
             Md=blkdiag(Md,md); 
            end
       end



       
        Ma=zeros(nd,obj.N);
       for kk=1:nd
           Ma(kk,24*(kk-1)+1:24*kk)=ones(1,24);
       end

       Cvp=cEV2/length(obj.hsi);
       Cvpv=repmat(Cvp,24,1);
       Cvpv(hnsi,:)=0;
       Pev2d=Cvpv(:);
         
         
       end

   end
    
end