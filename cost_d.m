function C=cost_d(Pdg,obj)

          %% Costs
        
          %% Diesel costs
          
          %%Unguia Diesel generators : 2 of 745 kVA
          %%Dcg=Cop+Cmant
          %%Fuel consumption for Diesel plants [ltr/kWh]: 0.2461 Pdg+0.084151 Pn_dg 
          Con_com=(1/3.78541).*((0.2461.*Pdg)+(0.084151*obj.dg.Ndg*obj.dg.Pdgnom.*ones(obj.r_l.Tf-obj.r_l.Ti,1))); %% Diesel fuel consumption [gal]
          cost_c=obj.dg.cost_c(obj.r_l.Ti+1:obj.r_l.Tf)*(8760/(obj.r_l.Tf-obj.r_l.Ti));%% Levelized cost for the calculation period
          Ccomb=cost_c'*Con_com;
          %%Lubricant consumption:0.001226 gal/kWh
          % % Dcg=Cop+Cmant
          Con_lub=0.001226.*Pdg; %% lubricant consumption
          cost_lub=obj.dg.cost_l*ones(obj.r_l.Tf-obj.r_l.Ti,1)*(8760/(obj.r_l.Tf-obj.r_l.Ti)); %% lubricant cost for Ungu�a
          Club=cost_lub'*Con_lub;
          %%Diesel operative costs
          Cop=Ccomb+Club;
          %%Maintenance costs
          Cmant_dg=obj.dg.cmt_dg*obj.dg.Ndg;
          Cdg=sum(Cop)+sum(Cmant_dg);
          
                    %% Diesel emissions
          %% Due to construction 
          E_dgc=obj.dg.Edgcc*obj.dg.Pdgnom*obj.dg.Ndg*((obj.prj_p.in*(obj.prj_p.in+1)^obj.prj_p.n)/(-1+(obj.prj_p.in+1)^obj.prj_p.n));
          %% Due to operation
          Con_com=(1/3.78541).*((0.2461.*Pdg)+(0.084151*obj.dg.Ndg*obj.dg.Pdgnom.*ones(obj.r_l.Tf-obj.r_l.Ti,1))); %% Diesel fuel consumption [gal]
          Edgop=obj.dg.Edgopc*3.78541*sum(Con_com)*(8760/(obj.r_l.Tf-obj.r_l.Ti));
          Edg=E_dgc+Edgop;
          
          C=sum(Cdg)+sum(obj.prj_p.wem*Edg);
end