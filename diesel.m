classdef diesel
    
    
   properties 
    
      Ndg 
      Pdgnom
      ndiesel
      E_dg_max 
      cost_c
      cost_l
      cmt_dg
      Edgcc
      Edgopc


   end
   
   methods
      
       function obj = diesel(Ndg, Pdgnom, ndiesel, E_dg_max , cost_c, cost_l, cmt_dg, Edgcc, Edgopc)
         obj.Ndg = Ndg;
         obj.Pdgnom = Pdgnom;
         obj.ndiesel = ndiesel;
         obj.E_dg_max = E_dg_max;
         obj.cost_c = cost_c;
         obj.cost_l = cost_l;
         obj.cmt_dg = cmt_dg;
         obj.Edgcc = Edgcc;
         obj.Edgopc = Edgopc;

       end
            
   end
    
end