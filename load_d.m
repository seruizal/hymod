%%% dise�o con casos semanales

function L = load_d(Nh)    
load('La');%% Base system load demand [kW]
Lai=reshape(La.*2.4429./385.2133,365,24);

for hh=1:365
Lai_max(hh)=max(Lai(hh,:));
end


%% Daily average load curve

Lm = [1.4404;1.2310;1.1231;1.0596;0.9835;0.9137;0.8947;1.5546;1.7640;...
1.4023;1.3706;1.4721;1.4975;1.4721;1.4531;1.6751;2.1320;2.4302;...
2.4429;2.2969;2.0051;1.5863;1.3388;1.2056];

MLm=max(Lm);

%% Maximum load values each 
%January 
Lai_maxa{1}=Lai_max(1:31);
%February
Lai_maxa{2}=Lai_max(31+1:31+28);
%March
Lai_maxa{3}=Lai_max(31+28+1:31+28+31);
%April
Lai_maxa{4}=Lai_max(31+28+31+1:31+28+31+30);
%May
Lai_maxa{5}=Lai_max(31+28+31+30+1:31+28+31+30+31);
%June
Lai_maxa{6}=Lai_max(31+28+31+30+31+1:31+28+31+30+31+30);
%July
Lai_maxa{7}=Lai_max(31+28+31+30+31+30+1:31+28+31+30+31+30+31);
%August
Lai_maxa{8}=Lai_max(31+28+31+30+31+30+31+1:31+28+31+30+31+30+31+31);
%September
Lai_maxa{9}=Lai_max(31+28+31+30+31+30+31+31+1:31+28+31+30+31+30+31+31+30);
%October
Lai_maxa{10}=Lai_max(31+28+31+30+31+30+31+31+30+1:31+28+31+30+31+30+31+31+30+31);
%November
Lai_maxa{11}=Lai_max(31+28+31+30+31+30+31+31+30+31+1:31+28+31+30+31+30+31+31+30+31+30);
%December
Lai_maxa{12}=Lai_max(31+28+31+30+31+30+31+31+30+31+30+1:31+28+31+30+31+30+31+31+30+31+30+31);

%%%% Statistic evaluation

for jj=1:12
pdfLm{jj}=fitdist(Lai_maxa{jj}','Normal');
end


ys=[];
for ii=1:12
y{ii} = random(pdfLm{ii}, size(Lai_maxa{ii}));
ys=[ys,y{ii}];
end

L=[];

for jj=1:365
    Ls=Lm.*(Nh).*(ys(jj)./MLm);
   
    L=[L;Ls];
end
return

