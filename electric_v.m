classdef electric_v
    
    
   properties 
    
      sigmaev
      Enomev
      Eminev
      Cch_nomev
      hsi 
      hpi 
      rev2 
      lEV2 
      Nev
      Nev1
      Nev2
      cinv_ev

   end
   
   methods
      
       function obj = electric_v(sigmaev, Enomev, Eminev, Cch_nomev, hsi, hpi, rev2,...
         lEV2, Nev, Nev1, Nev2, cinv_ev)
         obj.sigmaev = sigmaev;
         obj.Enomev = Enomev;
         obj.Eminev = Eminev;
         obj.Cch_nomev = Cch_nomev;
         obj.hsi = hsi;
         obj.hpi= hpi;
         obj.rev2 = rev2;
         obj.lEV2 = lEV2;
         obj.Nev = Nev;
         obj.Nev1 = Nev1;
         obj.Nev2 = Nev2;
         obj.cinv_ev = cinv_ev;
       end
            
   end
    
end