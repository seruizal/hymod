classdef hydro_p
    
    
   properties 
    
      np
      nt
      vol_min
      sigma1
      h
      Q_max 
      cinv_p
      cinv_t
      cinv_V
      cmt_hp
      Ehpcc

   end
   
   methods
      
       function obj = hydro_p(np, nt, vol_min, sigma1, h,...
         Q_max, cinv_p, cinv_t, cinv_V, cmt_hp, Ehpcc)
         obj.np = np;
         obj.nt = nt;
         obj.vol_min = vol_min;
         obj.sigma1= sigma1;
         obj.h = h;
         obj.Q_max = Q_max;
         obj.cinv_p = cinv_p;
         obj.cinv_t = cinv_t;
         obj.cinv_V = cinv_V;
         obj.cmt_hp = cmt_hp;
         obj.Ehpcc = Ehpcc;
       end
            
   end
    
end