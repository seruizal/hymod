classdef storage
    
    
   properties 
      
      N  
      %% Storage
      %%%Batteries
      sigma %% Self diacharging factor
      Enom  %% Maximum power that can be stored [kW*h]
      Emin %% Minimum discharge percentage [kW*h]


      %%%Hydro-pumped storage
      vol_min %% Initial volume
      sigma1 %% Evaporation losses
             
      %%EVs
      sigmaev %% Self diacharging factor of EVs batteries
      Enomev  %% kW*h
      Eminev %% Minimum discharge percentage
      
  %% Output matrices 
      b1
      Ar
      Arv
      v1
      Ar1
      bev
      
   end
   
   methods
      
      function obj = storage(sigma, sigmaev, Enom, Emin, vol_min, sigma1, Enomev, Eminev, r_l)
         obj.sigma = sigma;
         obj.sigmaev = sigmaev;
         obj.Enom = Enom;
         obj.Emin = Emin;
         obj.vol_min = vol_min;
         obj.sigma1 = sigma1;
         obj.Enomev = Enomev;
         obj.Eminev = Eminev;
         obj.N = r_l.N;
         [obj.b1,obj.Ar,obj.Arv,obj.v1,obj.Ar1,obj.bev] = storagem(obj);
       end
     
       function [b1,Ar,Arv,v1,Ar1,bev] = storagem(obj)
         
          %%Matrix energy equation in the battery and in the water storage tank

          b12=[];
          b12b=[];
          for k=1:(obj.N)+1
              b1a=(1-obj.sigma)^(k-1);
              b1b=(1-obj.sigma1)^(k-1);
              b12=[b12;b1a];
              b12b=[b12b;b1b];
          end
          b12v=b12;
          
          flg4 = getappdata(0,'flg4');
          flg5 = getappdata(0,'flg5');
          flg6 = getappdata(0,'flg6');
                    
        if flg4==0
            obj.Emin=0;
            b12=zeros((obj.N)+1,1);
            Ar=ones(obj.N+1,obj.N);
        end
        
        if flg5==0
            obj.vol_min=0;
            b12b=zeros((obj.N)+1,1);
            Ar1=ones(obj.N+1,obj.N);
        end
        
        if flg6==0
            obj.Eminev=0;
            b12v=zeros((obj.N)+1,1);
            Arv=ones(obj.N+1,obj.N);
        end
          b1=obj.Emin*b12;
          bev=obj.Eminev*b12v;
          v1=obj.vol_min*b12b;

         
if flg4~=0
          Ap=ones(obj.N+1,obj.N);
          Aa=Ap;
          Ae=tril(Ap,-1);
          Ara=tril(Ap,-1)-tril(Ap,-2);
          [row,col]=find(Ara==1);
          Ar=Ara;
         s=2;
          while s<obj.N+1
             row=row(2:end);
             col=col(2:end)-ones(length(col)-1,1);
             jj=sub2ind(size(Ar),row,col);
             
             Ar(jj)=(1-obj.sigma)^(s-1);
          s=s+1;
          
          end
end
if flg5~=0
          Ap=ones(obj.N+1,obj.N);
          Aa=Ap;
          Ae=tril(Ap,-1);
          Ara=tril(Ap,-1)-tril(Ap,-2);
          [row,col]=find(Ara==1);
          Ar1=Ara;
    s=2;
          while s<obj.N+1
             row=row(2:end);
             col=col(2:end)-ones(length(col)-1,1);
             jj=sub2ind(size(Ar),row,col);
             
             Ar1(jj)=(1-obj.sigma1)^(s-1);
          s=s+1;
          
          end
end 
if flg6~=0
        s=2;
         Ap=ones(obj.N+1,obj.N);
          Aa=Ap;
          Ae=tril(Ap,-1);
          Ara=tril(Ap,-1)-tril(Ap,-2);
          [row,col]=find(Ara==1);
          Arv=Ara;
          while s<obj.N+1
             row=row(2:end);
             col=col(2:end)-ones(length(col)-1,1);
             jj=sub2ind(size(Ar),row,col);
             
             Arv(jj)=(1-obj.sigmaev)^(s-1);
          s=s+1;
          
          end
end
         
       end

   end
    
end